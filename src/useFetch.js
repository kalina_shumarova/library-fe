import React from "react";
import { useState, useEffect } from "react";

const useFetch = (url, options, initialState,{ isRendered, setIsRendered}) => {

    const [ state, setState ] = useState(initialState);
    const [ isLoading, setIsLoading ] = useState(false);
    // const [ isRendered, setIsRendered ] = useState(false);

    useEffect(() => {
        setIsLoading(true);

    fetch(url, options)
      .then((res) => res.json())
      .then(result => {
        setState(result);
        setIsLoading(false);

        if (!isRendered && isRendered !== undefined) {
          setIsRendered(true);
        }

      })
      .catch(error => {console.log('Error with fetching API', error)});
    }, []);
  
    return [ state, isLoading ];

}

export default useFetch;