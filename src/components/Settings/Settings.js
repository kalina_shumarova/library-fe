import React from "react";
import { useState } from "react";
import Header from "../Header/Header";
import "./Settings.css";

export default function Settings() {
  const generalData = [
    { title: "Notifications and emails" },
    { title: "User Management" },
    { title: "Physical Libraries" },
    { title: "Reading Events" },
    { title: "Invoicing" },
    { title: "Book Statistics" },
    { title: "Readers Statistics" },
    { title: "Events Statistics" },
  ];

  const bookData = [
    { title: "Manage Settings" },
    { title: "Book Visibility" },
    { title: "Authors Database" },
    { title: "Book Covers" },
    { title: "Book Covers" },
  ];

  // generate unique keys
  const uniqueId = require("lodash.uniqueid");
  const arrowImg = require("./Polygon 48.svg").default;

  generalData.forEach((setting) => {
    setting.uniqueKey = uniqueId();
  });

  bookData.forEach((setting) => {
    setting.uniqueKey = uniqueId();
  });

  return (
    <>
      <div className="layout">
        <Header />
        <div className="settings-wrapper">
          <div className="general-settings">
            <div className="headings">
              <h2>General Settings</h2>
            </div>

            <ul className="settings-list">
              {generalData.map((data) => (
                <div className="setting-wrapper">
                  <li key={data.uniqueKey} className="setting">
                    {data.title}
                  </li>
                  <span className="arrow">
                    <span className="arrow-line"></span>
                    <img src={arrowImg}></img>
                  </span>
                </div>
              ))}
            </ul>
          </div>

          <div className="book-settings">
            <div className="headings">
              <h2>Book Settings</h2>
              <div>
                <button>Add New</button>
              </div>
            </div>

            <ul className="settings-list">
              {generalData.map((data) => (
                <div className="setting-wrapper">
                  <li key={data.uniqueKey} className="setting">
                    {data.title}
                  </li>
                  <span className="arrow">
                    <span className="arrow-line"></span>
                    <img src={arrowImg}></img>
                  </span>
                </div>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </>
  );
}
