import React from "react";
import "./MobileNav.css";
import { Link, NavLink } from "react-router-dom";
import "./Header.css";
import { IoClose } from "react-icons/io5";

export default function MobileNav({ logo, isRendered }) {
  let screenSizeCheck = window.matchMedia("screen and (max-width: 1000px)");


  return (
    <div className="mobile-container">
      <nav className="mobile-dropdown">
        <div className="mobile-top-bar">
          <Link to="/library" className="nav-close">
            <IoClose />
          </Link>
          
          <Link to="/library" className="mobile-logo-wrapper-nav">
          {/* {screenSizeCheck.matches && isRendered &&
            (<img src={logo} className="logo" alt="DIGI BOOKS"></img>)
          } */}
            <img src={logo} className="mobile-logo" alt="DIGI BOOKS"></img>
          </Link>
        </div>

        <ul className="mobile-nav">
          <NavLink to="/library" className={({ isActive }) =>
              "mobile-nav" + (isActive ? " active" : "")}>
            Library
          </NavLink>
          <NavLink to="/settings" className={({ isActive }) =>
              "mobile-nav settings" + (isActive ? " active" : "")}>
            Settings
          </NavLink>
        </ul>
      </nav>
    </div>
  );
}
