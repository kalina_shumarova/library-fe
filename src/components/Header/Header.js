import React from "react";
import { useState, useRef, useEffect } from "react";
import { NavLink, Link, useNavigate, Navbar } from "react-router-dom";
import "./Header.css";
import MobileNav from "./MobileNav";
import "./MobileNav.css";
import { act } from "react-dom/test-utils";
import ConditionalNav from "./ConditionalNav";

export default function Header({ isRendered, isLoggedIn }) {
  const logo = require("../Login&Signup/graphics/Exclusion 1.png");
  const userImg = require("./profile.svg").default;
  const menuImg = require("./menu.svg").default;
  const backArrow = require("./Polygon 39.svg").default;
  const navigate = useNavigate();

  const handleLogout = () => {
    // localStorage.clear();
    console.log(localStorage);
    navigate("/");
  };

  // let screenSizeCheck = window.matchMedia("screen and (max-width: 1000px)");

  return (
    <div className="header-wrapper">
      <nav className="header-nav">
        {isRendered ? (
          <Link to="/library" className="book-header">
          <div
            className="book-header-button responsive-library-button"
            onClick={() => {
              navigate("/library");
            }}
          >
            <img src={backArrow}></img>
          </div>
          <span>Library</span>
        </Link>
        ) : (
          <Link to="/library" className="logo-wrapper-nav">
          <img src={logo} className="logo" alt="DIGI BOOKS"></img>
        </Link>
        )}

        {/* <ul className="nav-wrapper"> */}
        {/* <li>
            {isRendered ? <NavLink to="/library"
              className="nav active">Library</NavLink> :
              <NavLink
              to="/library"
              className={({ isActive }) => "nav" + (isActive ? " active" : "")}
            >
              Library
            </NavLink>
              }
          </li>
          <li>
            <NavLink
              to="/settings"
              className={({ isActive }) =>
                "nav settings" + (isActive ? " active" : "")
              }
            >
              Settings
            </NavLink>
          </li> */}

        <ConditionalNav isRendered={isRendered} />

        {/* <NavLink to="/library" className="nav" activeClassName="active">Library</NavLink>
             <NavLink to="/settings" className="nav" activeClassName="active">Settings</NavLink> */}
        {/* </ul> */}

        <span className="nav-responsive">
          <img src={menuImg}></img>
          {/* {screenSizeCheck.matches &&
            (<img src={logo} className="logo" alt="DIGI BOOKS"></img>)
          } */}
          <MobileNav logo={logo} isRendered={isRendered} />
        </span>

        {isRendered && (
          <Link to="/library" className="logo-wrapper-nav mobile-logo">
          <img src={logo} className="logo" alt="DIGI BOOKS"></img>
        </Link>
        )}

        <span className="user-icon">
          <img src={userImg} alt="User Icon"></img>
          <div className="dropdown">
            <button type="button" onClick={handleLogout}>
              Log out
            </button>
          </div>
        </span>
      </nav>
    </div>
  );
}
