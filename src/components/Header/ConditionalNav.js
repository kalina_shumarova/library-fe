import React from "react";
import { NavLink } from "react-router-dom";

const ConditionalNav = ({ isRendered }) => {
  if (isRendered) {
    return (
      <ul className="nav-wrapper">
        <li>
          <NavLink to="/library" className="nav active">
            Library
          </NavLink>
        </li>

        <li>
          <NavLink to="/settings" className="nav settings">
            Settings
          </NavLink>
        </li>
      </ul>
    );
  } else {
    return (
      <ul className="nav-wrapper">
        <li>
          <NavLink
            to="/library"
            className={({ isActive }) => "nav" + (isActive ? " active" : "")}>
            Library
          </NavLink>
        </li>

        <li>
          <NavLink to="/settings" className={({ isActive }) =>
                "nav settings" + (isActive ? " active" : "")}>
            Settings
          </NavLink>
        </li>
      </ul>
    );
  }
};

export default ConditionalNav;
