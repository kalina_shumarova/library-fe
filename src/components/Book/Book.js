import React from "react";
import { useState, useEffect } from "react";
import { useParams, Link, useNavigate } from "react-router-dom";
import "./Book.css";
import BookHeader from "./BookHeader";
import useFetch from "../../useFetch";
import Header from "../Header/Header";

export default function Book({ isRendered, setIsRendered }) {
  const { id } = useParams();
  const url = `https://books-library-dev.herokuapp.com/api/book/${id}`;
  const token = localStorage.getItem("user-info");
  const backArrow = require("../Header/Polygon 39.svg").default;
  const navigate = useNavigate();

  const bookInitialState = {
    _id: "",
    name: "",
    author: "",
    image: "",
    createOn: "",
    lastUpdateOn: "",
    genre: { _id: "", name: "" },
  };

  const options = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };
  

  const [book, isLoading] = useFetch(url, options, bookInitialState, { isRendered, setIsRendered});
  // setIsRendered(true);

  console.log(book);
  console.log(isRendered);

  return (
    <>
      <div className="layout-book">
        {/* <BookHeader /> */}
        <Header isRendered={isRendered}/>

        {isLoading ? (
          <div>Loading...</div>
        ) : (
          <div className="page-wrapper">
            {/* library button */}
             <Link to="/library" className="library-btn-mobile">
          <div
            className="library-btn-mobile responsive-library-button"
            onClick={() => {
              navigate("/library");
            }}
          >
            <img src={backArrow}></img>
          </div>
          <span>Library</span>
        </Link>

            <section className="img-wrapper">
              <img src={book.image}></img>
            </section>

            <section className="title-info-wrapper">
              <div className="title">
                <h2>{book.name}</h2>
              </div>

              <div className="author">
                <h3>{book.author}</h3>
              </div>

              <div className="details">
                <div className="genre">
                  Genre: <b>&nbsp;{book.genre.name}</b>
                </div>
                <div>
                  Created on:{" "}
                  <b>
                    &nbsp;
                    {book.createOn
                      .toString()
                      .split("T")[0]
                      .split("-")
                      .reverse()
                      .join(".")}
                  </b>
                </div>
                <div>
                  {" "}
                  Updated on:{" "}
                  <b>
                    &nbsp;
                    {book.lastUpdateOn
                      .toString()
                      .split("T")[0]
                      .split("-")
                      .reverse()
                      .join(".")}
                  </b>
                </div>
              </div>

              <div className="description-wrapper">
                <h4>Short description</h4>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                  irure dolor in reprehenderit in voluptate velit esse cillum
                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                  cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </p>
              </div>
            </section>
          </div>
        )}
      </div>
    </>
  );
}
