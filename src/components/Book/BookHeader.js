import React from "react";
import { useState, useRef, useEffect } from "react";
import { NavLink, Link, useNavigate, useNavigation, Navbar,} from "react-router-dom";
import "./BookHeader.css";

export default function BookHeader() {

  const userImg = require("../Header/profile.svg").default;
  const backArrow = require("../Header/Polygon 39.svg").default;
  const navigate = useNavigate();

  const handleLogout = () => {
    // localStorage.clear();
    console.log(localStorage);
    navigate("/");
  };

  return (
       <div className="header-wrapper">
      <nav className="header-nav">
        <Link to="/library" className="book-header">
         <div className="book-header-button" onClick={() => {
           navigate("/library")
         }}><img src={backArrow}></img></div>
         <span>Library</span>
        </Link>
        <ul className="nav-wrapper">
          <li>
            <NavLink
              to="/library"
              className="nav active"
            >
              Library
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/settings"
              className="nav"
            >
              Settings
            </NavLink>
          </li>
        </ul>
        <span className="user-icon">
          <img src={userImg} alt="User Icon"></img>
          <div className="dropdown">
            <button type="button" onClick={handleLogout}>
              Log out
            </button>
          </div>
        </span>
      </nav>
    </div>
  )
}
