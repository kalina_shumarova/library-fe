import React from "react";
import { Route, Link } from "react-router-dom";
import SignUp from "../Login&Signup/SignUp";
import Library from "../Library/Library";

function ProtectedRoute() {
  const isAuthenticated = localStorage.getItem("isAuthenticated");
//   console.log("this", isAuthenticated);

  return (
    <Route
      element={ isAuthenticated ? <SignUp /> : <Library /> }
    />
  );
}

export default ProtectedRoute;