import React from "react";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { BsEyeFill } from "react-icons/bs";
import "./Login&Signup.css";
import Login from "./Login.js";
import { FaEye } from "react-icons/fa";

export default function SignUp() {
  const logo = require("./graphics/Exclusion 1.png");
  const backgroundImg = require("./graphics/Group 19445.png");
  const navigate = useNavigate();
  const eyeIcon = require("./graphics/view.svg").default;
  const [isPasswordShown, setIsPasswordShown] = useState(false);


  const url = "https://books-library-dev.herokuapp.com/api/user/register";

  let user = {
    username: "elka",
    password: "123456"
  };

  // request options
  const options = {
    method: "POST",
    body: JSON.stringify(user),
    headers: {
      "Content-Type": "application/json",
    }
  }

  async function signup() {
    const response = await fetch(url, options);
    const json = await response.json();
    const result = localStorage.setItem("user-info", json);
    console.log(result);
  }

  const handleSubmit = async e => {
    e.preventDefault();
    signup();
    console.log(localStorage.getItem("user-info"));
    navigate("/");
  }

  const togglePasswordBtn = () => {
    setIsPasswordShown(!isPasswordShown);
  }

  // disable button 

  const [ isDisabled, setIsDisabled ] = useState(true);

  const handleDisabled = (e) => {
    if (e.target.value !== "") {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }

  return (
    <>
      <div className="layout">
        <div className="container">
          <div className="login-page-wrapper">
            <div className="signup-logo">
              <img src={logo} className="logo" alt="DIGI BOOKS"></img>
            </div>

            <h2 className="signup-title">Welcome to the best book database!</h2>
            <h3 className="signup-subtitle">Create your profile</h3>

            <form className="form-login-signup mobile-signup">
            <h2 className="signup-title mobile">Welcome to the best book database!</h2>
            <h3 className="signup-subtitle mobile">Create your profile</h3>
              <div className="signup">
                <div className="input-label signup">
                  <label>Email</label>
                  <input type="text" onChange={(e) => handleDisabled(e)}></input>
                </div>

                <div className="input-label">
                  <label>Password</label>

                  <div className="password-input">
                    <input type={isPasswordShown ? "text" : "password"}
                           onChange={(e) => handleDisabled(e)}></input>
                    <FaEye className={isPasswordShown ? "icon shown" : "icon"} onClick={togglePasswordBtn}/>
                  </div>
                </div>

                <div className="input-label">
                  <label>Repeat password</label>

                  <div className="password-input repeat-password">
                    <input type={isPasswordShown ? "text" : "password"}
                          onChange={(e) => handleDisabled(e)}></input>
                    <FaEye className={isPasswordShown ? "icon shown" : "icon"} onClick={togglePasswordBtn}/>
                  </div>
                </div>
              </div>

              <div className={isDisabled ? "btn-signup disabled" : "btn-signup"}>
                <div>
                  <button type="submit" onClick={handleSubmit} disabled={isDisabled}>Sign up</button>
                </div>

                <p>
                  You have an account?
                  <Link to="/" className="login-here-btn">
                    Log in here
                  </Link>
                </p>
              </div>
            </form>
          </div>

          <img src={backgroundImg} className="bgr-img" alt="Library"></img>
        </div>
      </div>
    </>
  );
}
