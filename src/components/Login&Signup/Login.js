import React from "react";
import { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./Login&Signup.css";
import { FaEye } from "react-icons/fa";

export default function Login({ isLoggedIn, setIsLoggedIn }) {
  const logo = require("./graphics/Exclusion 1.png");
  const backgroundImg = require("./graphics/Group 19445.png");
  // const eyeIcon = require("./graphics/view.png");

  const navigate = useNavigate();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isPasswordShown, setIsPasswordShown] = useState(false);
  const [ isDisabled, setIsDisabled ] = useState(true);
  // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNjgyZmI4NjgwMzZjN2JjMGQyMTA1MSIsInVzZXJuYW1lIjoiZWxrYSIsImlhdCI6MTY0Njc2ODE2NSwiZXhwIjoxNjQ2ODU0NTY1fQ.KfldXi408lt1a_jW59QcnqXhGVurakth42hjTMJhW_k";

  const url = "https://books-library-dev.herokuapp.com/api/user/login";

  let user = {
    username: "elka",
    password: "123456",
  };

  // request options
  const options = {
    method: "POST",
    body: JSON.stringify(user),
    headers: {
      "Content-Type": "application/json"
    },
  };

  async function login() {
    const response = await fetch(url, options);
    const json = await response.json();
    const res = json["token"];
    const result = localStorage.setItem("user-info", res);
    console.log(result);
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    login();
    // setIsLoggedIn(true);
    navigate("/library");
  };


  const togglePasswordBtn = () => {
    setIsPasswordShown(!isPasswordShown);
  }

  const handleDisabled = (e) => {
    if (e.target.value !== "") {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }

  // const [ error, setError ] = useState();
  // const submit = (e) => {
  //   e.preventDefault();

  //   //if username or password field is empty, return error message
  //   if (username === "" || password === "") {
  //     setError("Empty username field");

  //   } else if (
  //     username.toLowerCase() === "elka" &&
  //     password === "123456"
  //   ) {
  //     //Signin Success
  //     localStorage.setItem("isAuthenticated", "true");
  //     window.location.pathname = "/library";
  //   } else {
  //     //If credentials entered is invalid
  //     setError("Invalid username/password");
  //     return;
  //   }
  // };

  return (
    <>
     <div className="layout">
      <div className="container">
        <div className="login-page-wrapper">
          <div className="login-logo">
            <img src={logo} alt="DIGI BOOKS"></img>
          </div>
          <h2 className="login-title">Welcome back!</h2>

          <form className="form-login-signup" onSubmit={handleSubmit}>
          <h2 className="login-title mobile">Welcome back!</h2>
            <div className="login">
              <div className="input-label">
                <label>Email</label>
                <input
                  type="text"
                  onChange={(e) => setUsername(e.target.value)}
                ></input>
              </div>

              <div className="input-label">
                <label>Password</label>

                <div className="password-input">
                  <input
                    type={isPasswordShown ? "text" : "password"}
                    onChange={(e) => {setPassword(e.target.value);
                                      handleDisabled(e) }}
                  ></input>
                  <FaEye className={isPasswordShown ? "icon shown" : "icon"} onClick={togglePasswordBtn}/>
                </div>
              </div>

              <div className="recover-password">
                <a href="#">Recover password</a>
              </div>
            </div>

            <div className={isDisabled ? "login-signup disabled" : "login-signup"}>
              <div>
                <button type="submit" disabled={isDisabled}>Log in</button>
              </div>

              <p>
                You don't have an account?
                <Link to="/signup" className="signup">
                  Sign up here
                </Link>
              </p>
            </div>
          </form>
        </div>

        <img src={backgroundImg} className="bgr-img" alt="Library"></img>
      </div>
    </div>
    </>
  );
}
