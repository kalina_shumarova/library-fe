import { getDefaultNormalizer } from "@testing-library/react";
import React, { useEffect, useState, useRef } from "react";
import { useNavigate, Link } from "react-router-dom";
import Header from "../Header/Header";
import "./Library.css";
import "../../App.css";

export default function Library() {
  const navigate = useNavigate();
  const searchIcon = require("./search-icon.svg").default;
  const buttonIcon = require("./Polygon 23.svg").default;

  //fetch all books
  const url = "https://books-library-dev.herokuapp.com/api/book";
  const [books, setBooks] = useState([]);
  const token = localStorage.getItem("user-info");

  const options = {
    // mode: 'no-cors',
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  };

  console.log(token);

  // const renderAllBooks = async () => {
  //   const response = await fetch(url, options);
  //   const json = await response.json();
  //   setBooks(json);
  // };


  const renderAllBooks = () => {
    fetch(url, options)
      .then((res) => res.json())
      .then(result => {setBooks(result);
        console.log(books)
      })
      .catch(error => {console.log('Error with fetching API', error)});
    }

  useEffect(async () => {
    renderAllBooks();
  }, []);



  //search by title
  const [input, setInput] = useState("");
  const searchUrl = "https://books-library-dev.herokuapp.com/api/book/search";

  let handleInput = (e) => {
    let lowerCase = e.target.value.toLowerCase();
    setInput(lowerCase);
  };

  let result = { pattern: input };

  const searchTitleOptions = {
    method: "POST",
    body: JSON.stringify(result),
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  };

  const searchByTitle = async (e) => {
    e.preventDefault();
    if (!input) {
      return renderAllBooks();
    } else {
      const response = await fetch(searchUrl, searchTitleOptions);
      const json = await response.json();
      setBooks(json); //array with book obj

      // for (let bookInfo of books) {
      //   const title = bookInfo.name.toLowerCase();
      //   if (title.match(/`${input}`/g)) {
      //     setBooks([...bookInfo]);
      //     console.log(books);
      //   }
      // }

      console.log(books);
    }
  };

  return (
    <>
      <div className="layout">
        <Header />
        <div className="library-wrapper">
          <section className="title-search-wrapper">
            <h2 className="library-title">All books</h2>
            <form className="search-wrapper" onSubmit={searchByTitle}>
              <input
                type="text"
                placeholder="Search"
                value={input}
                onChange={handleInput}
              />
              <img src={searchIcon}></img>
            </form>
          </section>

          <section className="books-wrapper">
            {books.map((book) => {
              return (
                <div className="book-section-wrapper">
                  <div key={book._id} className="book">
                    <img src={book.image}></img>

                    <div className="book-info-wrapper">
                      <h3 className="book-title">{book.name}</h3>
                      <h4 className="book-author">{book.author}</h4>
                      <div className="book-genre" key={book.genre._id}>
                        Genre: <b>{book.genre.name}</b>
                      </div>
                      <div className="book-details">
                        <div>
                          Created on:{" "}
                          <b>
                            &nbsp;
                            {book.createOn
                              .toString()
                              .split("T")[0]
                              .split("-")
                              .reverse()
                              .join(".")}
                          </b>
                        </div>
                        <div>
                          {" "}
                          Updated on:{" "}
                          <b>
                            &nbsp;
                            {book.lastUpdateOn
                              .toString()
                              .split("T")[0]
                              .split("-")
                              .reverse()
                              .join(".")}
                          </b>
                        </div>
                      </div>
                    </div>
                  </div>

                  <button onClick={() => navigate(`/book/${book._id}`)}>
                    <img src={buttonIcon}></img>
                  </button>
                  
                </div>
              );
            })}
          </section>
        </div>
      </div>
    </>
  );
}
