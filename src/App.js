import "./App.css";
import { useState } from "react";
import { BrowserRouter, Routes as Switch, Route } from "react-router-dom";
import Login from "./components/Login&Signup/Login";
import SignUp from "./components/Login&Signup/SignUp";
import Library from "./components/Library/Library";
import Settings from "./components/Settings/Settings";
import Book from "./components/Book/Book";
// import Layout from "./components/Layout/Layout";

function App() {

  const [ isRendered, setIsRendered ] = useState(false); //check if Book page is rendered
  const [ isLoggedIn, setIsLoggedIn ] = useState(false);

  return (
    <div className="App">
      <BrowserRouter>
        {/* <Layout isRendered={isRendered} isLoggedIn={isLoggedIn}> */}
          <Switch>
            <Route path="/" element={<Login isLoggedIn={isLoggedIn} setIsLoggedIn={setIsLoggedIn}/>} />
            <Route path="/signup" element={<SignUp />} />
            
            <Route path="/library" element={<Library/>} />
            <Route path="/settings" element={<Settings/>} />
            <Route path="/book/:id" element={<Book isRendered={isRendered} setIsRendered={setIsRendered}/>} />
          </Switch>
        {/* </Layout> */}
      </BrowserRouter>
    </div>
  );
}

export default App;
